pg8000==1.29.8
SQLAlchemy==2.0.24
gunicorn==20.1.0
google-cloud-storage==2.16.0 
fastapi==0.111.0
psycopg2-binary==2.9.9 