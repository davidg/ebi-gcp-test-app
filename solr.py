from urllib.request import *
import json
import os

SOLR_HOST=os.environ["SOLR_HOST"]
SOLR_COLLECTION=os.environ["SOLR_COLLECTION"]

def solr_query():
  connection = urlopen(f"http://{SOLR_HOST}:8983/solr/{SOLR_COLLECTION}/select?q=cheese&wt=json")
  response = json.load(connection)
  return f"{response['response']['numFound']} documents found."
