from sqlalchemy import Column, Integer, DateTime, String
from sqlalchemy.sql import func
from database import Base

class Visits(Base):
    __tablename__ = "visits"
    id = Column(Integer, primary_key=True, autoincrement=True)
    request_metadata = Column(String, nullable=False)
    date = Column(DateTime(timezone=True), server_default=func.now())