from fastapi import Depends, FastAPI, Request
from sqlalchemy.orm import Session
from fastapi.responses import HTMLResponse
from fastapi.templating import Jinja2Templates

import crud, models, schemas
from database import SessionLocal, engine
from storage import get_objects_from_bucket
from solr import solr_query
models.Base.metadata.create_all(bind=engine)

app = FastAPI()
templates = Jinja2Templates(directory="templates")

# Dependency
def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()

@app.get("/solr", response_class=HTMLResponse)
def solr_handler(request: Request):
  solr_documents = solr_query()
  return templates.TemplateResponse(
      request=request, name="solr.html", context={"solr_documents": solr_documents}
  )

@app.get("/visits", response_class=HTMLResponse)
def create_visit(request: Request, db: Session = Depends(get_db)):
  visits = crud.get_visits(db)
  return templates.TemplateResponse(
      request=request, name="visits.html", context={"visits": visits}
  )

@app.get("/storage", response_class=HTMLResponse)
def create_visit(request: Request, db: Session = Depends(get_db)):
  objects = get_objects_from_bucket()
  return templates.TemplateResponse(
      request=request, name="storage.html", context={"objects": objects}
  )

@app.get("/", response_class=HTMLResponse)
def create_visit(request: Request, db: Session = Depends(get_db)):
  request_metadata = request.headers.__repr__()
  crud.create_visit(db, request_metadata=request_metadata)
  return templates.TemplateResponse(
      request=request, name="home.html", context={"request_metadata": request_metadata}
  )