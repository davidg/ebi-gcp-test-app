from google.cloud import storage
import os

def get_objects_from_bucket():
  """
    Instantiates a client, the client relies on the GOOGLE_APPLICATION_CREDENTIALS
    you can create service accounts to test in your local machine buit avoid to push
    code to develop and production environments referencing service accounts in local
    files, instead use workload identity on GKE or attach the service account to the
    resource hosting the app
  """
  bucket_name = os.environ["BUCKET_NAME"]
  storage_client = storage.Client()
  blobs = storage_client.list_blobs(bucket_name)
  objects = []
  for blob in blobs:
      objects.append(blob.name)
  return objects