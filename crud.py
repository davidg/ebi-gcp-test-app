from sqlalchemy.orm import Session
import models

# create a visit record on the database
def create_visit(db: Session, request_metadata: str):
    visit = models.Visits(request_metadata=request_metadata)
    db.add(visit)
    db.commit()
    db.refresh(visit)
    return visit

# get the first 5 visits records 
def get_visits(db: Session):
    return db.query(models.Visits).order_by(models.Visits.date.desc()).limit(5).all()