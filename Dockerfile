FROM python:3

COPY requirements.txt ./

RUN set -ex; \
    pip install -r requirements.txt; \
    pip install uvicorn

EXPOSE 8080

ENV APP_HOME /app
ENV PORT 8080

WORKDIR $APP_HOME
COPY . ./

CMD ["uvicorn", "main:app", "--host", "0.0.0.0", "--port", "8080", "--reload"]